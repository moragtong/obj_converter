extern crate obj;
extern crate cgmath;

use cgmath::InnerSpace;
use std::io::Write;

#[repr(C)]
struct Header {
	vert_size:		u64,
	radius:			f32,
}

#[repr(C)]
struct PerVertex {
	pub position:	[f32; 3],
	pub normal:		[f32; 3],
	pub tangent:	[f32; 3],
	pub uv:			[f32; 2],
}

pub fn build_adjacancy_list(indices: &[obj::SimplePolygon]) -> Vec<[u32; 6]> {
	let mut edges = std::collections::HashMap::new();

	for tri in indices.iter() {
		edges.insert((tri[0].0, tri[1].0), tri[2].0);
		edges.insert((tri[2].0, tri[0].0), tri[1].0);
		edges.insert((tri[1].0, tri[2].0), tri[0].0);
	}

	indices.iter().map(|tri| [
		tri[0].0 as _,
		edges[&(tri[1].0, tri[0].0)] as _,
		tri[1].0 as _,
		edges[&(tri[2].0, tri[1].0)] as _,
		tri[2].0 as _,
		edges[&(tri[0].0, tri[2].0)] as _,
	]).collect()
}


fn generate(obj: &obj::Obj<obj::SimplePolygon>, indices: &[obj::SimplePolygon]) -> Vec<PerVertex> {
	let positions = &obj.position;
	let uv_coords = &obj.texture;

	let mut normals_tangents = Vec::new();
	normals_tangents.resize(positions.len(), Vec::with_capacity(3));

	let mut uvs = Vec::new();
	uvs.resize(positions.len(), None);

	for tri in indices.iter() {
		let normal = (cgmath::Vector3::from(positions[tri[0].0]) - cgmath::Vector3::from(positions[tri[1].0]))
			.cross(cgmath::Vector3::from(positions[tri[1].0]) - cgmath::Vector3::from(positions[tri[2].0]));

		let tangent = {
			let e1 = cgmath::Vector3::from(positions[tri[1].0]) - cgmath::Vector3::from(positions[tri[0].0]);
			let e2 = cgmath::Vector3::from(positions[tri[2].0]) - cgmath::Vector3::from(positions[tri[0].0]);
			let delta_uv1 = cgmath::Vector2::from(uv_coords[1]) - cgmath::Vector2::from(uv_coords[0]);
			let delta_uv2 = cgmath::Vector2::from(uv_coords[2]) - cgmath::Vector2::from(uv_coords[0]);

			cgmath::Vector3 {
				x:	delta_uv2.y * e1.x - delta_uv1.y * e2.x,
				y:	delta_uv2.y * e1.y - delta_uv1.y * e2.y,
				z:	delta_uv2.y * e1.z - delta_uv1.y * e2.z,
			} / (delta_uv1.x * delta_uv2.y - delta_uv2.x * delta_uv1.y)
		};

		for vertex in tri.iter() {
			if let Some(index) = vertex.1 {
				uvs[vertex.0].get_or_insert(uv_coords[index]);
			}
			normals_tangents[vertex.0].push((normal, tangent));
		}
	}

	positions.iter().zip(normals_tangents.iter()).zip(uvs).map(|((position, normals_tangents), uv)| {
		let normals_sum: cgmath::Vector3<_> = normals_tangents.iter().map(|(normal, _)| normal).sum();
		let tangents_sum: cgmath::Vector3<_> = normals_tangents.iter().map(|(_, tangent)| tangent).sum();

		PerVertex {
			position:	*position,
			normal:		(normals_sum / normals_tangents.len() as f32).normalize().into(),
			tangent:	(tangents_sum / normals_tangents.len() as f32).normalize().into(),
			uv:			uv.unwrap()
		}
	}).collect()
}

fn generate_bounding_sphere(per_vertices: &[PerVertex]) -> f32 {
	let mut radius = 0.;
	for per_vertex in per_vertices {
		let mag = cgmath::Vector3::from(per_vertex.position).magnitude();
		if mag > radius {
			radius = mag;
		}
	}
	
	dbg!(radius)
}

fn main() -> std::io::Result<()> {
	for name in std::env::args().skip(1) {
		let filename = std::path::PathBuf::from(&name);
		let obj = obj::Obj::<obj::SimplePolygon>::load(&filename)?;
		let indices = &obj.objects[0].groups[0].polys;

		let mut output = std::fs::File::create(name.rsplit('.').nth(1).unwrap().to_string() + ".mesh")?;

		let vertices = generate(&obj, &indices);

		let header = Header {
			vert_size:		(obj.position.len() * std::mem::size_of::<PerVertex>()) as _,
			radius:			generate_bounding_sphere(&vertices),
		};

		output.write(unsafe { std::slice::from_raw_parts(&header as *const _ as _, std::mem::size_of::<Header>()) })?;

		let indices = build_adjacancy_list(&indices);

		output.write(unsafe { std::slice::from_raw_parts(vertices.as_ptr() as _, header.vert_size as _) })?;
		output.write(unsafe { std::slice::from_raw_parts(indices.as_ptr() as _, (indices.len() * std::mem::size_of::<[u32; 6]>()) as _) })?;
	}

	Ok(())
}
